const fs = require('fs');
function User(id,login,role,fullname,registeredAt,avaUrl,isDisabled){
    this.id = id;
    this.login = login;
    this.role = role;
    this.fullname = fullname;
    this.registeredAt = registeredAt;
    this.avaUrl = avaUrl;
    this.isDisabled = isDisabled;
}
let fakeUsers;
fs.readFile('./data/users.json','utf8', (err, data) => {
    if (err) throw err;
    //console.log(data);
    let dataUs = JSON.parse(data, function(key, value) {
        if (key === 'registeredAt') return new Date(value);
        return value;
        });
    //console.log(dataUs);
    fakeUsers = dataUs.items;
  });

module.exports = {
    getById:function(id){
        return fakeUsers.find(x=>x.id === id);
    },
    getAll:function(){
        return fakeUsers;
    }
};