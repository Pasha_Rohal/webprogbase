const message = "This is lab3!";
console.log(message);
const User = require("./models/user");
const Story = require("./models/story");
// npm i consolidate swig
const { ServerApp, ConsoleBrowser, InputForm } = require("webprogbase-console-view");
const consolidate = require('consolidate');
const path = require('path');
const express = require('express');
const mustache =require("mustache-express");
const app = express();
//
// view engine setup
app.use(express.static('public'));

app.engine("mst",mustache(path.join(__dirname,"views","partials")));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'mst');
//get
app.get('/', function (req, res) {
    res.render('index',{ });
});
app.get ("/users",function (req,res) {
    const users = User.getAll();
    if(!users){res.statusCode = 404;res.end();return;}
    res.render("users",{users:users});
});
app.get ("/tests",function (req,res) {
    const st = Story.getAll();
    if(!st){res.statusCode=404;res.end();return;}
    res.render("tests",{st:st});

});
app.get('/about', function (req, res) {
    res.render('about',{ });
});
app.get('/users', function (req, res) {
    res.render('users',{ });
});
app.get("/api/users", function (req, res) {
    res.send(User.getAll());
});

app.get("/api/users/:id", function (req, res) {

    res.send(User.getById(Number(req.params.id)));
});

app.get ("/users/:id",function (req,res) {
    const user = User.getById(Number(req.params.id));
    if(!user){res.statusCode = 404;res.end();return;}
    res.render("user",user);
});
app.get ("/tests/:id",function (req,res) {
    const tasks = Story.getById(Number(req.params.id));
    if(!tasks){res.end();res.statusCode = 404;return;}
    res.setHeader("Content-type","text/html");
    res.render("tasks",tasks);
});

app.use(function () { console.log('Any request'); });
app.listen(3002, function () { console.log('Server is ready'); });






























// const app2 = new ServerApp();
// const browser = new ConsoleBrowser();
// app2.use("/", function (req, res) {
//     const links = {
//         "users": "Users submenu",
//         "story": "Story submenu",
//     };
//     res.send("Hello", links);
// });
// app2.use("users", function (req, res) {
//     const links = {
//         "allUsers": "Show all users",
//         "getUser": "Select user",
//     };
//     res.send("User submenu is here!<", links);
// });
// app2.use("story", function (req, res) {
//     const links = {
//         "allStory": "Show all story",
//         "getStory": "Select story",
//         "insert": "Insert story",
//         "update": "update story",
//         "delete": "delete story",
//     };
//     res.send("Story submenu is here!", links);
// });

// app2.use("allUsers", function (req, res) {
//     const users = User.getAll();//get users
//     let usersListText = "";
//     for (let user of users) {
//         usersListText += `*${user.id} ${user.fullname}\r\n`;
//     }
//     res.send(usersListText);
// });
// app2.use("getUser", function (req, res) {
//     //create user input form
//     const fields = {
//         "id": "Enter user id",

//     };
//     const nextState = "showUser";
//     const form = new InputForm(nextState, fields);
//     res.send("Select user!", form);
// });

// app2.use("showUser", function (req, res) {
//     //get user id from request
//     const userIdStr = req.data.id; //user ID is string;
//     const userId = parseInt(userIdStr);//@todo checks;
//     const user = User.getById(userId);
//     if (!user) {
//         res.send(`${undefined}`);
//         return;
//     }
//     res.send(`You have chosen user with id ${userId}:\r\nLogin ${user.login}\r\nFullname ${user.fullname}\r\nRole ${user.role}\r\nIsoDate ${user.registeredAt}\r\nURL picture ${user.avaUrl}\r\nDisabled ${user.isDisabled}`);
// });
// app2.use("allStory", function (req, res) {
//     const storys = Story.getAll();//get users
//     let storyListText = "";
//     for (let story of storys) {
//         storyListText += `*${story.id} ${story.name}\r\n`;
//     }
//     res.send(storyListText);
// });
// app2.use("getStory", function (req, res) {
//     //create user input form
//     const fields = {
//         "id": "Enter user id",
//     };
//     const nextState = "showStory";
//     const form = new InputForm(nextState, fields);
//     res.send("Select story!", form);
// });
// app2.use("showStory", function (req, res) {
//     const StoryIdStr = req.data.id; //user ID is string;
//     const storyId = parseInt(StoryIdStr);//@todo checks;
//     const story = Story.getById(storyId);
//     if (!story) {
//         res.send(`${undefined}`);
//         return;
//     }
//     res.send(`You have chosen story with id ${storyId}:\r\nname ${story.name}\r\nRaiting ${story.raiting}\r\ngenre ${story.genre}\r\nIsoDate ${story.registeredStory}\r\nNumber ${story.number}`);
// });
// app2.use("update", function (req, res) {
//     const name = {
//         "id": "Enter  id",
//         "name": "Enter name",
//         "genre": "Enter genre",
//         "number": "Enter number",
//         "raiting": "Enter raiting",
//     };
//     const nextState = "updateStory";
//     const form = new InputForm(nextState, name);
//     res.send("Enter id !", form);
// });
// app2.use("updateStory", function (req, res) {
//     const story = Story.getById(parseInt(req.data.id));

//     if (!story) {
//         res.send(`${undefined}`);

//     } else {
//         if (isNaN(parseInt(req.data.number)) || isNaN(parseInt(req.data.raiting))) {
//             res.send(`${undefined}`);
//         } else {
//             const numberStr = req.data.number;
//             const raitingStr = req.data.raiting;
//             const storyNumber = parseInt(numberStr);
//             const storyRaiting = parseInt(raitingStr);
//             story.name = req.data.name;
//             story.genre = req.data.genre;
//             story.number = storyNumber;
//             story.raiting = storyRaiting;
//             Story.update(story);
//             res.send(`You updated a story with id ${story.id}:\r
//     Name ${story.name}\r
//     genre ${story.genre}\r
//     number ${story.number}\r
//     raiting ${(story.raiting)}\r
//     RegisteredAt ${story.registeredStory}\r`);
//         }
//     }
// });
// app2.use("delete", function (req, res) {
//     const fields = {
//         "id": "Enter  id",
//     };
//     const nextState = "deleteStory";
//     const form = new InputForm(nextState, fields);
//     res.send("Select Story!", form);
// });
// app2.use("deleteStory", function (req, res) {
//     const storyIdStr = req.data.id; //story ID is string;
//     const storyId = parseInt(storyIdStr);//@todo checks;
//     if (!Story.getById(storyId)) {
//         res.send(`${undefined}`);
//     } else {
//         Story.delete(storyId);
//         res.send(`You deleted story with id ${storyId}`);
//     }
// });
// app2.use("insert", function (req, res) {
//     const name = {
//         "name": "name story",
//         "genre": "genre",
//         "raiting": "raiting",
//         "number": "number",
//     };
//     const nextState = "insertStory";
//     const form = new InputForm(nextState, name);
//     res.send("Enter!", form);
// });
// app2.use("insertStory", function (req, res) {
//     const story = Story.nStory(req.data.name, req.data.genre, req.data.raiting, req.data.number);
//     if (!story) {
//         res.redirect("insert Story");
//         return;
//     }
//     if (isNaN(parseInt(req.data.number)) || isNaN(parseInt(req.data.raiting))) {
//         res.send(`${undefined}`);
//     } else {
//         const newId = Story.insert(story);

//         res.send(`You created a story with id ${newId}:\r\nName ${story.name}\r
//     genre ${story.genre}\r
//     number ${story.number}\r
//     Raiting ${story.raiting}\r
//     RegisteredStory ${story.registeredStory}\r`);
//     }
// });

