const fs = require('fs');
function Story(id, name, genre, raiting, number, registeredStory) {
    this.id = id;
    this.name = name;
    this.genre = genre;
    this.raiting = raiting;
    this.number = number;
    this.registeredStory = registeredStory;
}
let fakeStory;
let nextId = 0;
fs.readFile('./data/story.json', 'utf8', (err, data) => {
    if (err) throw err;
    //console.log(data);
    let dataUs = JSON.parse(data, function (key, value) {
        if (key === 'registeredStory') return new Date(value);
        return value;
    });
    //console.log(dataUs);
    fakeStory = dataUs.items;
    nextId = dataUs.nextId;
});

 function writeToFile() {
    const items = fakeStory ;
    const x = {
    nextId,
    items,
    };
    fs.writeFile('./data/story.json', JSON.stringify(x), (err) => {
    if (err) throw err;
    });
}


module.exports = {
    getById: function (id) {
        return fakeStory.find(x => x.id === id);
    },
    getAll: function () {
        return fakeStory;
    },
    insert: function (x) {
        fakeStory.splice(nextId, 0, x);
        nextId++;
        writeToFile();
        return nextId - 1;
    },
    //доделать/ update
    
    update(x) {
        //fakeStory.splice(x.id,1,x);
        writeToFile();

    },
    delete: function (id) {
        const lastId = fakeStory.indexOf(this.getById(id));
        fakeStory.splice(lastId,1);
        writeToFile();
    },
    getNextId: function () {
        return nextId;
    },
    nStory: function (name, genre, number, raiting) {
        const dt = (new Date());
        const valible = new Story(nextId, name, genre, parseInt(number), parseInt(raiting), dt);
        return valible;
    }
};
