const message = "This is lab4!";
console.log(message);
const User = require("./models/user");
const Story = require("./models/story");
// npm i consolidate swig
const { ServerApp, ConsoleBrowser, InputForm } = require("webprogbase-console-view");
const consolidate = require('consolidate');
const path = require('path');
const express = require('express');
const mustache = require("mustache-express");
const paginate = require('express-paginate');
const urlencodedBodyParser = require('body-parser');
const bodyParser = require('busboy-body-parser');
const fs = require('fs');
const app = express();

app.use(express.static('public'));
app.use(bodyParser({ limit: '3mb' }));
app.use(express.static(path.join(__dirname, 'data/fs')));
app.use(urlencodedBodyParser.urlencoded({ extended: false }));
app.engine("mst", mustache(path.join(__dirname, "views", "partials")));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'mst');
//get
app.get('/', function (req, res) {
    res.render('index', {});
});
app.get("/users", function (req, res) {
    User.getAll((err, users) => {
        if (err) {
            res.status(500).send(err.toString());
        }
        if (!users) { res.statusCode = 404; res.end(); return; }
        res.render("users", { users: users });
    });
});
app.get("/tests", function (req, res) {
    Story.getAll((err, st) => {
        if (err) {
            res.status(500).send(err.toString());
        }
        let last = 0;
        let page = req.query.page;
        let search = "";
        const sort = [];
        if(req.query.search){
            search = req.query.search;
        }
        if(req.query.p){
            search = req.query.p;
        }
        
        if (page === undefined && search ==="") {
            res.redirect("/tests?page=1")
            return;
        }else if(page === undefined){
            res.redirect(`/tests?page=1&search=${search}`);
            return;
        }
        page = parseInt(page);
        
        const story = [];
        if(search === ""){
            for (let i = (page - 1) * 5; i < page * 5 && i < st.length; i++) {
                story.push(st[i]);
            }
            last = Math.ceil(st.length / 5);
        }else{
            for(let i = 0;i < st.length;i++){
                
                const name = st[i].name.toUpperCase();
                if(name.includes(search.toUpperCase())){
                    sort.push(st[i]);
                }
            }
            for (let i = (page - 1) * 5; i < page * 5 && i < sort.length; i++) {
                story.push(sort[i]);
            }
            last =  Math.ceil(sort.length / 5);
        }
        
        const next = page + 1;
        const prev = page - 1; 
        const First = page != 1;
        const Thelast = next - 1 != last;
        res.render("tests", { st: story, page, Thelast, next, prev,last, First, isNull:last === 0,search})
    });
});

app.post('/addStory', (req, res) => {
    
    const name = req.body.name;
    const raiting = Number.parseInt(req.body.raiting);
    const number = Number.parseInt(req.body.number);
    const genre = req.files.genre;
    const registeredStory = new Date(req.body.registeredStory).toISOString();
    const photoStatus = genre.name.lastIndexOf('.') < 0 ? "" : genre.name;
    const storys = new Story(0,name,photoStatus,raiting,number,registeredStory);
    Story.insert(storys,(err,id)=>{
        if(err){
            res.status(500).send(err.toString());
            return;
        }
        fs.writeFile(`./data/fs/${genre.name}`, genre.data, (err) => {
            if (err){
                res.status(500).send(err.toString());
                return;
            }
        });
        res.redirect(`/tests/${id}`);
    })
    
});

app.post('/remove', (req, res) => {
    const id = Number.parseInt(req.body.id);
    Story.delete(id,(err,data)=>{
        if(err){
            return res.status(500);
        }
        res.redirect('/tests');
    })
});

app.get('/story/new', (req, res) => {
    res.render('newstory', { });
});
app.get('/about', (req, res) => {
    res.render('about', { });
});

app.get("/api/users", (req, res) => {
    res.send(User.getAll());
});

app.get("/api/users/:id", (req, res) => {

    res.send(User.getById(Number(req.params.id)));
});

app.get("/users/:id", (req, res) => {
    User.getById(Number(req.params.id), (err, user) => {
        if (err) {
            res.status(500).send(err.toString());
        }
        if (!user) { res.sendStatus(404); res.end(); return; }
        res.render("user", user);
    });
});
app.get("/tests/:id", (req, res) => {
    Story.getById(Number(req.params.id), (err, tasks) => {
        if (err) {
            res.status(500).send(err.toString());
        }
        if (!tasks) { res.sendStatus(404);res.end(); return; }
        res.setHeader("Content-type", "text/html");
        res.render("tasks", tasks);
    });
});

app.use(function () { console.log('Any request'); });
app.listen(3000, function () { console.log('Server is ready'); });






























// const app2 = new ServerApp();
// const browser = new ConsoleBrowser();
// app2.use("/", function (req, res) {
//     const links = {
//         "users": "Users submenu",
//         "story": "Story submenu",
//     };
//     res.send("Hello", links);
// });
// app2.use("users", function (req, res) {
//     const links = {
//         "allUsers": "Show all users",
//         "getUser": "Select user",
//     };
//     res.send("User submenu is here!<", links);
// });
// app2.use("story", function (req, res) {
//     const links = {
//         "allStory": "Show all story",
//         "getStory": "Select story",
//         "insert": "Insert story",
//         "update": "update story",
//         "delete": "delete story",
//     };
//     res.send("Story submenu is here!", links);
// });

// app2.use("allUsers", function (req, res) {
//     const users = User.getAll();//get users
//     let usersListText = "";
//     for (let user of users) {
//         usersListText += `*${user.id} ${user.fullname}\r\n`;
//     }
//     res.send(usersListText);
// });
// app2.use("getUser", function (req, res) {
//     //create user input form
//     const fields = {
//         "id": "Enter user id",

//     };
//     const nextState = "showUser";
//     const form = new InputForm(nextState, fields);
//     res.send("Select user!", form);
// });

// app2.use("showUser", function (req, res) {
//     //get user id from request
//     const userIdStr = req.data.id; //user ID is string;
//     const userId = parseInt(userIdStr);//@todo checks;
//     const user = User.getById(userId);
//     if (!user) {
//         res.send(`${undefined}`);
//         return;
//     }
//     res.send(`You have chosen user with id ${userId}:\r\nLogin ${user.login}\r\nFullname ${user.fullname}\r\nRole ${user.role}\r\nIsoDate ${user.registeredAt}\r\nURL picture ${user.avaUrl}\r\nDisabled ${user.isDisabled}`);
// });
// app2.use("allStory", function (req, res) {
//     const storys = Story.getAll();//get users
//     let storyListText = "";
//     for (let story of storys) {
//         storyListText += `*${story.id} ${story.name}\r\n`;
//     }
//     res.send(storyListText);
// });
// app2.use("getStory", function (req, res) {
//     //create user input form
//     const fields = {
//         "id": "Enter user id",
//     };
//     const nextState = "showStory";
//     const form = new InputForm(nextState, fields);
//     res.send("Select story!", form);
// });
// app2.use("showStory", function (req, res) {
//     const StoryIdStr = req.data.id; //user ID is string;
//     const storyId = parseInt(StoryIdStr);//@todo checks;
//     const story = Story.getById(storyId);
//     if (!story) {
//         res.send(`${undefined}`);
//         return;
//     }
//     res.send(`You have chosen story with id ${storyId}:\r\nname ${story.name}\r\nRaiting ${story.raiting}\r\ngenre ${story.genre}\r\nIsoDate ${story.registeredStory}\r\nNumber ${story.number}`);
// });
// app2.use("update", function (req, res) {
//     const name = {
//         "id": "Enter  id",
//         "name": "Enter name",
//         "genre": "Enter genre",
//         "number": "Enter number",
//         "raiting": "Enter raiting",
//     };
//     const nextState = "updateStory";
//     const form = new InputForm(nextState, name);
//     res.send("Enter id !", form);
// });
// app2.use("updateStory", function (req, res) {
//     const story = Story.getById(parseInt(req.data.id));

//     if (!story) {
//         res.send(`${undefined}`);

//     } else {
//         if (isNaN(parseInt(req.data.number)) || isNaN(parseInt(req.data.raiting))) {
//             res.send(`${undefined}`);
//         } else {
//             const numberStr = req.data.number;
//             const raitingStr = req.data.raiting;
//             const storyNumber = parseInt(numberStr);
//             const storyRaiting = parseInt(raitingStr);
//             story.name = req.data.name;
//             story.genre = req.data.genre;
//             story.number = storyNumber;
//             story.raiting = storyRaiting;
//             Story.update(story);
//             res.send(`You updated a story with id ${story.id}:\r
//     Name ${story.name}\r
//     genre ${story.genre}\r
//     number ${story.number}\r
//     raiting ${(story.raiting)}\r
//     RegisteredAt ${story.registeredStory}\r`);
//         }
//     }
// });
// app2.use("delete", function (req, res) {
//     const fields = {
//         "id": "Enter  id",
//     };
//     const nextState = "deleteStory";
//     const form = new InputForm(nextState, fields);
//     res.send("Select Story!", form);
// });
// app2.use("deleteStory", function (req, res) {
//     const storyIdStr = req.data.id; //story ID is string;
//     const storyId = parseInt(storyIdStr);//@todo checks;
//     if (!Story.getById(storyId)) {
//         res.send(`${undefined}`);
//     } else {
//         Story.delete(storyId);
//         res.send(`You deleted story with id ${storyId}`);
//     }
// });
// app2.use("insert", function (req, res) {
//     const name = {
//         "name": "name story",
//         "genre": "genre",
//         "raiting": "raiting",
//         "number": "number",
//     };
//     const nextState = "insertStory";
//     const form = new InputForm(nextState, name);
//     res.send("Enter!", form);
// });
// app2.use("insertStory", function (req, res) {
//     const story = Story.nStory(req.data.name, req.data.genre, req.data.raiting, req.data.number);
//     if (!story) {
//         res.redirect("insert Story");
//         return;
//     }
//     if (isNaN(parseInt(req.data.number)) || isNaN(parseInt(req.data.raiting))) {
//         res.send(`${undefined}`);
//     } else {
//         const newId = Story.insert(story);

//         res.send(`You created a story with id ${newId}:\r\nName ${story.name}\r
//     genre ${story.genre}\r
//     number ${story.number}\r
//     Raiting ${story.raiting}\r
//     RegisteredStory ${story.registeredStory}\r`);
//     }
// });

