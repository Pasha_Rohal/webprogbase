const fs = require('fs');

// function writeToFile(){
//     fs.writeFile("./data/story.json", JSON.stringify(st, null, 4), error => {
//         if (error) {return callback(new Error(`Error write`));}
//     });
// }

function File(fp, callback) {
    fs.readFile(fp, (error, data) => {
        if (error) {return callback(new Error("Error reading"));}
        try {
            data = JSON.parse(data.toString());
        } catch (error) {return callback(new Error("Error parsing"));}
        const Storys = new Array();
        for (const story of data.items) {
            Storys.push(new Story(
                story.id,
                story.name,
                story.genre,
                story.raiting,
                story.number,
                story.registeredStory,
            ));
        }
        data.items = Storys;
        callback(null, data);
    });
}

class Story {

    constructor(id, name, genre, raiting, number, registeredStory) {
        this.id = id;
        this.name = name;
        this.genre = genre;
        this.raiting = raiting;
        this.number = number;
        this.registeredStory = registeredStory;
    }

    static insert(x, callback) {
        File("./data/story.json", (err, st) => {
            if (err) {
                return callback(err);
            }
            x.id = st.nextId++;
            x.registeredStory = new Date(x.registeredStory);
            x.registeredStory = x.registeredStory.toISOString();
            st.items.push(x);
            fs.writeFile("./data/story.json", JSON.stringify(st, null, 4), error => {
                if (error) {return callback(new Error(`Error write`));}
                callback(null, x.id);
            });
        });
    }
    static getAll(callback) {
        File('./data/story.json', (err, st) => {
            if (err) {
                return callback(err);
            }
            callback(null, st.items);
        });
    }
    static getById(id, callback) {
        this.getAll((err, st) => {
            if (err) { return callback(err); }
            callback(null, st.find(x => {
                return x.id === id;
            }));
        });
    }
    static delete(id, callback) {
        File("./data/story.json", (err, st) => {
            if (err) {return callback(err);}
            const index = st.items.findIndex(x => {return x.id === id;});
            st.items.splice(index, 1);
            fs.writeFile("./data/story.json", JSON.stringify(st, null, 4), error => {
                if (error) {return callback(new Error(`Error write`));}
                callback(null);
            });
        });
    }
}
module.exports = Story;