const fs = require('fs');
function getUsersStorageFromFile(fp, callback) {
    fs.readFile(fp, (Error, data) => {
        if (Error) {
            return callback(new Error('Error read'));
        }
        try {
            data = JSON.parse(data);
        } catch (e) {
            return callback(new Error('Error  parsing'));
        }
        const Users = [];
        for (const user of data.items) {
            Users.push(new User(user.id, 
                       user.login,
                        user.role,
                    user.fullname,
                user.registeredAt,
                      user.avaUrl,
                  user.isDisabled));
        }
        data.items = Users;
        callback(null, data);
    }); 
}

class User {

    constructor(id, login, role, fullname, registeredAt, avaUrl, isDisabled) {
        this.id = id;
        this.login = login;
        this.role = role;
        this.fullname = fullname;
        this.registeredAt = registeredAt;
        this.avaUrl = avaUrl;
        this.isDisabled = isDisabled;
    }

    static getAll(callback) {
        getUsersStorageFromFile('./data/users.json', (error, users) => {
            if (error) {
                return callback(error);
            }
            callback(null, users.items);
        });
    }

    static getById(id, callback) {
        this.getAll((err, users) => {
            if (err) {
                return callback(err);
            }
            callback(null, users.find(cur => {
                return cur.id === id;
            }));
        });
    }
}

module.exports = User;