const message = "This is lab5!";
console.log(message);

const Story = require("./models/story");
const Actual = require("./models/actual");
const User = require("./models/user");
// npm i consolidate swig
const path = require('path');
const express = require('express');
const mustache = require("mustache-express");
const urlencodedBodyParser = require('body-parser');
const bodyParser = require('busboy-body-parser');
const mongoose = require('mongoose');
const fs = require('fs');
const app = express();


app.use(express.static('public'));
app.use(bodyParser({ limit: '3mb' }));
app.use(express.static(path.join(__dirname, 'data/fs')));
app.use(urlencodedBodyParser.urlencoded({ extended: false }));
app.engine("mst", mustache(path.join(__dirname, "views", "partials")));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'mst');

const usersRouter = require('./routes/users');
app.use("/users", usersRouter);

const storyRouter = require('./routes/tests');
app.use("/tests", storyRouter);
//get
app.get('/', function (req, res) {
    res.render('index', {});
});

app.post('/addStory', function (req, res) {

    const name = req.body.name;
    const raiting = Number.parseInt(req.body.raiting);
    const number = Number.parseInt(req.body.number);
    const genre = req.files.genre;
    const registeredStory = new Date(req.body.registeredStory).toISOString();
    const photoStatus = genre.name.lastIndexOf('.') < 0 ? "" : genre.name;
    const storys = new Story(0, name, photoStatus, raiting, number, registeredStory);

    Story.insert(storys)
        .then(x => {

            fs.writeFile(`./data/fs/${genre.name}`, genre.data, (err) => {
                if (err) throw err;
            });
            res.redirect(`/tests/${x._id}`)

        })
        .catch(err => res.status(500).send(err.toString()));
});

app.post('/remove', function (req, res) {
    Story.delete(req.body.id)
        .then(() => res.redirect('/tests'))
        .catch(err => res.status(500).send(err.toString()));
});

app.get('/:id/actual', function (req, res) {
    const id = req.params.id;
    Story.getAll()
        .then(story => {
            res.render("newactualpage", { story: story, id_1: req.params.id });
        })
        .catch(err => res.status(500).send(err.toString()));
});

app.get("/users/:id/actual/:id_1", function (req, res) {
    User.getById(req.params.id)
        .then(user => {
            Actual.insert(user.actual, req.params.id_1);
            return;
            User.update(user)
            
        })
        .then(() => res.redirect(`/users/${user.id}/actual`))
        .catch(err => res.status(500).send(err.toString()));
    
});

app.get("/users/:id/actual",function(req,res){
    User.getById(req.params.id)
    .then(user =>
        {
        Story.getAll()
        .then(story =>
            {
            const arrayUser = [];
            for(let i = 0;i < user.actual.length;i++)
            {
                for(let k = 0;k < story.length;k++)
                {
                    if(user.actual[i] === story[k].id)
                    {
                        arrayUser.push(story[k]);
                    }
                    
                }
            }
             res.render("downloadnewactul",{story: arrayUser, id_1:req.params.id });
        })
        .catch (err => res.status(500).send(err.toString()));
    })
    .catch (err => res.status(500).send(err.toString()));
});


app.get('/story/new', function (req, res) {
    res.render('newstory', {});
});
app.get('/downloadnewactul', function (req, res) {
    res.render('downloadnewactul', {});
});
app.get('/about', function (req, res) {
    res.render('about', {});
});

app.post("/users/:id", function (req, res) {
    const id = req.params.id;
    User.getById(id)
        .then((x) => {
            Actual.insert(id)
            res.redirect(`/users/${x._id}`)
        })
        .catch(err => res.status(500).send(err.toString()));
});

app.post("/users/:idl/actual/del/:id",function(req,res){
    User.getById(req.params.idl)
    .then(user =>{
        Actual.delete(user.actual,req.params.id);
        User.update(user)
    .then(()=>res.redirect(`/users/${user.id}`))
    .catch(err=>res.status(500).send(err.toString()));
    })
    .catch(err=>res.status(500).send(err.toString()));
})

const databaseUrl = 'mongodb://localhost:27017/lab5db';
const serverPort = 3004;
const connectOptions = { useNewUrlParser: true };
mongoose.connect(databaseUrl, connectOptions)
    .then(() => console.log(`Database connected: ${databaseUrl}`))
    .then(() => app.listen(serverPort, () => console.log(`Server started: ${serverPort}`)))
    .catch(err => console.log(`Start error: ${err}`));
