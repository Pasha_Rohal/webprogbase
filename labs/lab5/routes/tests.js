const express=require('express');

const Story = require('../models/story');
const routes = express.Router();

// routes.get("/:id", (req, res) => {
//     Story.getById(Number(req.params.id), (err, tasks) => {
//         if (err) {
//             res.status(500).send(err.toString());
//         }
//         if (!tasks) { res.sendStatus(404);res.end(); return; }
//         res.setHeader("Content-type", "text/html");
//         res.render("tasks", tasks);
//     });
// });
routes.get("/:id",function(req,res){
    const id = req.params.id;
    Story.getById(id)
    .then(story => {
        if(typeof story === "undefined"){
            res.status(404).send(`User with id ${id} not found`);
        }else{
            res.render("tasks",{story:story});
        }
    })
    .catch(err => res.status(500).send(err.toString()));
})

routes.get("/", function (req, res) {
    let last = 0;
         let page = req.query.page;
         let search = "";
         const sort = [];
         const story = [];
    Story.getAll()
        .then(st =>{
            if(req.query.search){
                search = req.query.search;
            }
            if(req.query.p){
                search = req.query.p;
            }
            if (page === undefined && search ==="") {
                res.redirect("/tests?page=1")
                return;
            }else if(page === undefined){
                res.redirect(`/tests?page=1&search=${search}`);
                return;
            }
            page = parseInt(page);
            if(search === ""){
                for (let i = (page - 1) * 5; i < page * 5 && i < st.length; i++) {
                    story.push(st[i]);
                    }
                    last = Math.ceil(st.length / 5);
            }else{
                for(let i = 0;i < st.length;i++){     
                const name = st[i].name.toUpperCase();
                    if(name.includes(search.toUpperCase())){
                        sort.push(st[i]);
                    }
                }
                for (let i = (page - 1) * 5; i < page * 5 && i < sort.length; i++) {
                    story.push(sort[i]);
                }
                last =  Math.ceil(sort.length / 5);
            }

        const next = page + 1;
        const prev = page - 1; 
        const First = page != 1;
        const Thelast = next - 1 != last;
        res.render("tests", { st: story, page, Thelast, next, prev,last, First, isNull:last === 0,search})
        
        } )
        .catch(err => res.status(500).send(err.toString));
});

module.exports = routes;