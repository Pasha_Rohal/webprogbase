const express=require('express');
const User = require('../models/user');
const routes = express.Router();

routes.get("/", function (req, res) {
    User.getAll()
        .then(users => res.render("users",{users:users
        }))
        .catch(err =>res.status(500).send(err.toString()));

});
routes.get("/:id",function(req,res){
    const id = req.params.id;
    User.getById(id)
    .then(user => {
        if(typeof user === "undefined"){
            res.status(404).send(`User with id ${id} not found`);
        }else{
            res.render("user",{user:user});
        }
    })
    .catch(err => res.status(500).send(err.toString()));
})

module.exports = routes;