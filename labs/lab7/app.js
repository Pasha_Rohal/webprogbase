const message = "This is lab6!";
console.log(message);

const crypto = require("crypto");

const Story = require("./models/story");
const Actual = require("./models/actual");
const User = require("./models/user");
// npm i consolidate swig
const path = require('path');
const express = require('express');
const mustache = require("mustache-express");
const urlencodedBodyParser = require('body-parser');
const bodyParser = require('busboy-body-parser');
const mongoose = require('mongoose');
const fs = require('fs');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const cookieParser = require('cookie-parser');
const session = require('express-session');

const serverSalt = "45%sAlT_";


const app = express();

const config = require('./config');
const cloudinary = require('cloudinary');

cloudinary.config({
    cloud_name: config.cloudinary.cloud_name,
    api_key: config.cloudinary.api_key,
    api_secret: config.cloudinary.api_secret
});

app.use(express.static('public'));
app.use(bodyParser({ limit: '3mb' }));
app.use(express.static(path.join(__dirname, 'data/fs')));
app.use(urlencodedBodyParser.urlencoded({ extended: false }));
app.engine("mst", mustache(path.join(__dirname, "views", "partials")));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'mst');
app.use(cookieParser());
app.use(session({
    secret: "secret",
    resave: false,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

const usersRouter = require('./routes/users');
app.use("/users", usersRouter);

const storyRouter = require('./routes/tests');
app.use("/tests", storyRouter);

const authRouter = require('./routes/auth');
app.use("/", authRouter);

//get
app.get('/', checkAuth, function (req, res) {
    let admin = true;
    if(req.user.role === 0){
        admin = false;
    }
    res.render('index', { user: req.user, admin:admin });
});


function sha512(password, salt) {
    const hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    const value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
};

app.post('/addStory', function (req, res) {
    const fileObject = req.files.genre;
    const fileBuffer = fileObject.data;
    cloudinary.v2.uploader.upload_stream({ resource_type: 'raw' },
        function (error, result) {
            const name = req.body.name;
            const raiting = Number.parseInt(req.body.raiting);
            const number = Number.parseInt(req.body.number);
            const registeredStory = new Date(req.body.registeredStory).toISOString();
            const genre = result.url;
            const storys = new Story(0, name, genre, raiting, number, registeredStory);
            Story.insert(storys)
                .then(x => {
                    res.redirect(`/tests/${x._id}`)
                })
                .catch(err => res.status(500).send(err.toString()));
        })
        .end(fileBuffer);

});

app.post('/remove', function (req, res) {
    Story.delete(req.body.id)
        .then(() => res.redirect('/tests'))
        .catch(err => res.status(500).send(err.toString()));
});

app.get('/:id/actual', function (req, res) {
    const id = req.params.id;
    Story.getAll()
        .then(story => {
            res.render("newactualpage", { story: story, id_1: req.params.id, user: req.user });
        })
        .catch(err => res.status(500).send(err.toString()));
});

app.get("/users/:id/actual/:id_1", function (req, res) {
    User.getById(req.params.id)
        .then(user => {
            Actual.insert(user.actual, req.params.id_1);
            User.update(user)
                .then(() => res.redirect(`/users/${user.id}/actual`))
                .catch(err => res.status(500).send(err.toString()));
        })
        .catch(err => res.status(500).send(err.toString()));

});

app.get("/users/:id/actual", checkAuth, function (req, res) {
    User.getById(req.params.id)
        .then(user => {
            Story.getAll()
                .then(story => {
                    const arrayUser = [];
                    for (let i = 0; i < user.actual.length; i++) {
                        for (let k = 0; k < story.length; k++) {
                            if (user.actual[i] === story[k].id) {
                                arrayUser.push(story[k]);
                            }
                        }
                    }
                    res.render("downloadnewactul", { story: arrayUser, id_1: req.params.id, user: req.user });
                })
                .catch(err => res.status(500).send(err.toString()));
        })
        .catch(err => res.status(500).send(err.toString()));
});

app.post("/:id/changeRole", function (req, res) {
    const id = req.params.id;
    User.getAll()
        .then(users => {
            for (let i = 0; i < users.length; i++) {
                console.log(users[i].role);
                if (users[i].id === id) {
                    if (users[i].role === 0) {
                        users[i].role = 1;
                    } else {
                        users[i].role = 0;
                    }
                    const userss = new User(id, users[i].login, users[i].role, "second Admin", new Date(), users[i].password, "../images/users2.png", true, new Array());
                    User.update(userss)
                        .then(x => {
                            res.redirect(`/`);
                        })
                }
            }
            console.log("///////////////////////")
            for (let i = 0; i < users.length; i++) {
                console.log(users[i].role);
            }


        })
        .catch(err => res.status(500).send(err.toString()));


});
app.get('/story/new', function (req, res) {
    res.render('newstory', { user: req.user });
});

app.get('/downloadnewactul', function (req, res) {
    res.render('downloadnewactul', { user: req.user });
});
app.get('/about', function (req, res) {
    res.render('about', { user: req.user });
});

app.post("/users/:id", function (req, res) {
    const id = req.params.id;
    const admin = true;
    User.getById(id)
        .then((x) => {
            Actual.insert(id)
            res.redirect(`/users/${x._id}`, { user: req.user })
        })
        .catch(err => res.status(500).send(err.toString()));
});

app.post("/users/:idl/actual/del/:id", function (req, res) {
    User.getById(req.params.idl)
        .then(user => {
            Actual.delete(user.actual, req.params.id);
            User.update(user)
                .then(() => res.redirect(`/users/${user.id}`))
                .catch(err => res.status(500).send(err.toString()));
        })
        .catch(err => res.status(500).send(err.toString()));
})

app.get('/admin', checkAdmin, (req, res) => res.end('Admin page'));

app.get('/profile', checkAuth, (req, res) =>
    res.redirect(`/users/${req.user.id}`));



function checkAdmin(req, res, next) {
    if (!req.user) res.sendStatus(401); // 'Not authorized'
    else if (req.user.role !== 1) res.sendStatus(403); // 'Forbidden'
    else next();  // пропускати далі тільки аутентифікованих із роллю 'admin'
}

function checkAuth(req, res, next) {
    if (!req.user) return res.redirect(`/auth/registering`);  // 'Not authorized'
    next();  // пропускати далі тільки аутентифікованих
}


passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    User.getById(id)
        .then(user => {
            done(user ? null : 'No user', user);
        });
});


passport.use(new LocalStrategy(
    function (username, password, done) {
        let hash = sha512(password, serverSalt).passwordHash;
        console.log(username, password);
        User.getAll()
            .then(user => {
                for (let i = 0; i < user.length; i++) {
                    console.log(user[i]);
                    if (user[i].login == username) {
                        console.log(hash, user[i].password);
                        if (user[i].password == hash) {
                            done(null, user[i]);
                        }
                    }
                }
            })
            .catch((err) => done(err, false));
    }
));

const databaseUrl = config.DatabaseUrl;
const serverPort = config.ServerPort;
const connectOptions = { useNewUrlParser: true };

mongoose.connect(databaseUrl, connectOptions)
    .then(() => console.log(`Database connected: ${databaseUrl}`))
    .then(() => app.listen(serverPort, () => console.log(`Server started: ${serverPort}`)))
    .catch(err => console.log(`Start error: ${err}`));
