const fs = require('fs');
const mongoose = require('mongoose');
const Actual = require("./actual");

const userSchema = new mongoose.Schema({
    login:{type:String,require:true},
    fullname:{type:String},
    role: {type:Number},
    registeredAt:{type:Date,default:Date.now},
    password:{type:String},
    avaUrl: {type: String},
    isDisabled:{type:Boolean},
    actual:{type:Array},
    created:{type:Date,default:Date.now},
});
const UserModel = mongoose.model('User',userSchema);


class User {

    constructor(id, login, role, fullname, registeredAt,password, avaUrl, isDisabled,actual) {
        this.id = id;
        this.login = login;
        this.role = role;
        this.fullname = fullname;
        this.registeredAt = registeredAt;
        this.password = password;
        this.avaUrl = avaUrl;
        this.isDisabled = isDisabled;
        this.actual = actual;
    }

    static getAll() {
        return UserModel.find();
    }

    static getById(id) {
        return UserModel.findById(id);
    }
    static insert(user){
        return  new UserModel(user).save();
    }
    static update(user){
        return UserModel.update(
        {"_id": user.id},
        {"$set":{
        "login" : user.login,
        "role" : user.role,
        "fullname" : user.fullname,
        "registeredAt" : user.registeredAt,
        "password" : user.password,
        "avaUrl" : user.avaUrl,
        "isDisabled" : user.isDisabled,
        "actual" : user.actual
        }});
    }
    
}



module.exports = User;