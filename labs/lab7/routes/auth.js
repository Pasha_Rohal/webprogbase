const express = require('express');

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const cookieParser = require('cookie-parser');
const session = require('express-session');
const mustache = require("mustache-express");
const crypto = require("crypto");
const urlencodedBodyParser = require('body-parser');
const bodyParser = require('busboy-body-parser');

const User = require('../models/user');

const routes = express.Router();


const serverSalt = "45%sAlT_";

function sha512(password, salt) {
    const hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    const value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
};


routes.post('/registering', function (req, res) {
    const username = req.body.username;
    req.session.error = "incorrect data"
    let password = req.body.password;
    let hash = sha512(password, serverSalt).passwordHash;
    const passwordRepeat = req.body.passwordRepeat;
    if (password !== passwordRepeat) {
        res.redirect(`/auth/registering?e=' + encodeURIComponent('Incorrect username or password')`);
        return;
    } else if (password === passwordRepeat) {
        User.getAll()
        .then(users => {
            for (let i = 0; i < users.length; i++) {
                if (users[i].login === username) {
                        res.redirect('/auth/registering');
                        return;
                }
            }
            const user = new User(null, username, 0, "Tests", new Date(), hash, "../images/defaultuser.png", true, new Array())
        User.insert(user)
            .then(x => {
                res.redirect(`/auth/login`)
            })
            
        }).catch(err => res.status(500).send(err.toString()));
        
    }

});

routes.get('/auth/registering', function (req, res) {
    res.render('register', {});
});
routes.get('/auth/login', function (req, res) {
    res.render('login', {});
});

routes.post('/login',
    passport.authenticate('local', {
        successRedirect: '/',
        failureRedirect: `/auth/login?e=' + encodeURIComponent('Incorrect username or password')`
    }));

routes.post('/logout',
    (req, res) => {
        req.logout();
        res.redirect('/');
    });

    function checkAdmin(req, res, next) {
        if (!req.user) res.sendStatus(401); // 'Not authorized'
        else if (req.user.role !== 'admin') res.sendStatus(403); // 'Forbidden'
        else next();  // пропускати далі тільки аутентифікованих із роллю 'admin'
    }
    
    function checkAuth(req, res, next) {
        if (!req.user) return res.redirect(`/auth/registering`);  // 'Not authorized'
        next();  // пропускати далі тільки аутентифікованих
    }
    

module.exports = routes;