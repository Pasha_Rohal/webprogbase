const express=require('express');
const User = require('../models/user');
const routes = express.Router();

routes.get("/", checkAuth,function (req, res) {
    if(req.user.role == 0){
        res.redirect("/profile");
    }else{
    User.getAll()
        .then(users => res.render("users",{users:users, user:req.user
        }))
        .catch(err =>res.status(500).send(err.toString()));
    }
    });

routes.get("/:id",checkAuth,function(req,res){
    if(req.user.role === 1){
        req.user = false;
    }

    const id = req.params.id;
    User.getById(id)
    .then(users => {
        if(typeof users === "undefined"){
            res.status(404).send(`User with id ${id} not found`);
        }else{
            res.render("user",{users:users, user:req.user});
        }
    })
    .catch(err => res.status(500).send(err.toString()));
})

function checkAdmin(req, res, next) {
    if (!req.user) res.sendStatus(401); // 'Not authorized'
    else if (req.user.role !== 1) res.sendStatus(403); // 'Forbidden'
    else next();  // пропускати далі тільки аутентифікованих із роллю 'admin'
}

function checkAuth(req, res, next) {
    
    if (!req.user) return res.redirect(`/auth/registering`);  // 'Not authorized'
    next();  // пропускати далі тільки аутентифікованих
}


module.exports = routes;